import { Box, Stack } from "@chakra-ui/react";
import { Row, Col } from "antd";
import Head from "next/head";

import CardThird from "./CardThird";
import Limit from "../image/web.jpg";
import Hylbar from "../image/web1.jpg";
import Hurdan from "../image/web2.jpg";

export default function ThirdSection() {
  return (
    <Box px={{ base: 6, xl: 32 }}>
      <Stack alignItems={"center"}>
        <Row align="center">
          <Col lg={{ span: 8 }} sm={{span: 12}} xs={{span: 24}}>
            <CardThird image={Hylbar.src} content={"Ашиглахад хялбар"} />
          </Col>
          <Col lg={{ span: 8 }} sm={{span: 12}} xs={{span: 24}}>
            <CardThird image={Limit.src} content={"Лимит өндөр"} />
          </Col>
          <Col lg={{ span: 8 }} sm={{span: 12}} xs={{span: 24}}>
            <CardThird image={Hurdan.src} content={"Шийдвэрлэлт хурдан"} />
          </Col>
        </Row>
      </Stack>
    </Box>
  );
}
