import React from "react"
import {chakra, Box, Flex} from "@chakra-ui/react"

export default function ModalCard({content}) {
  return (
    <Flex px={{base: 12, sm: 16, md: 32}} py={6} w="full" alignItems="center" justifyContent="center">
      <Box
        mx="auto"
        px={8}
        py={4}
        rounded="3xl"
        shadow={"light-lg"}
        bg={"gray.800"}
        display={"flex"}
        justifyContent="center"
        alignItems="center"
        pos={"relative"}
        maxW="container.lg"
        minW={{"2xl": "480px", xl: "400px", lg: "450px", base: "full"}}
        h={{xl: "200px", lg: "180px", md: "150px", sm: "150px", base: "165px"}}
      >
        <Box mt={2}>
          <chakra.p
            textAlign={"justify"}
            fontSize={{base: "12px", md: "14px"}}
            color={"white"}
            fontWeight="500"
          >
            <span
              style={{
                fontSize: "60px",
                fontFamily: "Montserrat",
                fontWeight: 700,
                position: "absolute",
                left: "-1.25rem",
                top: "calc(50% - 40px)",
              }}
            >
              {content.substring(0, 1)}
            </span>
            {content.substring(2)}
          </chakra.p>
        </Box>
      </Box>
    </Flex>
  )
}
