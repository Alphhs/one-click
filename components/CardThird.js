import {Box, Center, Heading, Stack, Image} from "@chakra-ui/react"

export default function ProductSimple({content, image}) {
  return (
    <Center py={{base: 4, md: "8", lg: "10"}}>
      <Box
        role={"group"}
        p={6}
        maxW={{lg: "530px", base: "400px", }}
        w={"full"}
        className="hover"
        rounded={"lg"}
        pos={"relative"}
        zIndex={1}
      >
        <Box
          rounded={"lg"}
          mt={-12}
          pos={"relative"}
          height={{lg: "330px", md: "250px", sm: "125px"}}
      
        >
          <Image
            alt="cover"
            rounded={"lg"}
            className="shadow-stack-2 hover"
            height={{lg: "330px", md: "250px", base: "125px"}}
            width={{lg: "582px", md: "560px", base: "350px"}}
            objectFit={"cover"}
            src={image}
          />
        </Box>
        <Stack pt={"50px"} align={"left"}>
          <Heading
            fontSize={{base: "lg", sm: "lg", md: "xl", lg: "2xl"}}
            fontFamily={"heading"}
            fontWeight={"semibold"}
            color={"white"}
            textAlign={"center"}
          >
            {content}
          </Heading>
        </Stack>
      </Box>
    </Center>
  )
}
