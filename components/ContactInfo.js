import React from "react"
import {
  chakra,
  Box,
  SimpleGrid,
  Flex,
  useColorModeValue,
  Icon,
  Button,
  Text,
} from "@chakra-ui/react"
import {PhoneIcon, EmailIcon, Search2Icon} from "@chakra-ui/icons"
import {FaLocationArrow, FaFacebookF, FaPhone} from 'react-icons/fa'
export default function App() {
  const Feature = (props) => {
    return (
      <Box>
        <Icon boxSize={12} color={"white"} mb={4} fill="none">
          {props.icon}
        </Icon>
        <chakra.h3
          mb={3}
          fontSize="lg"
          lineHeight="shorter"
          fontWeight="bold"
          color={"white"}
        >
          {props.title}
        </chakra.h3>
        <chakra.p
          lineHeight="tall"
          color={useColorModeValue("gray.600", "gray.400")}
        >
          {props.children}
        </chakra.p>
      </Box>
    )
  }

  return (
    <Flex w="full" justifyContent="space-between" alignItems="center">
      <SimpleGrid
        columns={{base: 1, md: 2, lg: 3}}
        w="full"
        spacing={{base: 5, lg: 10, xl: 20}}
        mx="auto"
      >
        <a target="_blank" href="https://www.facebook.com/oneclick.mn">
          <Button
            colorScheme="whiteAlpha"
            variant="outline"
            borderWidth={"2px"}
            borderRadius={"xl"}
            px="lg"
            py="7"
            w={"full"}
            leftIcon={<Icon as={FaFacebookF} size={"1.4rem"} style={{transform: "translateY(-1px)"}} color={"white"} />}
          >
            <Text textColor={"white"} fontSize="lg" fontWeight={"normal"}>
              One Click
            </Text>
          </Button>
        </a>
        <a href="mailto:info@oneclick.mn">
          <Button
            colorScheme="whiteAlpha"
            variant="outline"
            borderWidth={"2px"}
            w={"full"}
            py="7"
            borderRadius={"xl"}
            leftIcon={<Icon as={EmailIcon} size={"1.2rem"} style={{transform: "translateY(-1px)"}}  color={"white"} />}
          >
            <Text textColor={"white"} fontSize="lg" fontWeight={"normal"}>
            info@oneclick.mn
            </Text>
          </Button>
        </a>
        <a
          href="tel:72700777 "
          target="_blank"
          rel="noreferrer"
        >
          <Button
            colorScheme="whiteAlpha"
            variant="outline"
            borderWidth={"2px"}
            w={"full"}
            py="7"
            borderRadius={"xl"}
            leftIcon={<Icon as={FaPhone} size={"1.2rem"} color={"white"} />}
          >
            <Text textColor={"white"} fontSize="lg" fontWeight={"normal"}>
            72700777 
            </Text>
          </Button>
        </a>
      </SimpleGrid>
    </Flex>
  )
}
