import React from "react"
import {
  chakra,
  Box,
  GridItem,
  useColorModeValue,
  Button,
  Stack,
  Center,
  Flex,
  Icon,
  SimpleGrid,
  VisuallyHidden,
  Input,
} from "@chakra-ui/react"
import CardSecond from "./CardSecond"
import FeatureSecond from "./FeatureSecond"

export default function App() {
  return (
    <Box zIndex={1} display={"flex"} justifyContent={"center"}>
      <SimpleGrid
        alignItems="flex-start"
        w={{base: "full", xl: 11 / 12}}
        columns={{base: 1, lg: 12}}
      >
        <GridItem
          colSpan={{base: "auto", lg: 7}}
          textAlign={{base: "left", lg: "left"}}
        >
          <FeatureSecond />
        </GridItem>
        <GridItem
          display={{base: "none", lg: "flex"}}
          colSpan={{base: "auto", md: 4}}
        >
          <CardSecond />
          <chakra.p fontSize="xs" textAlign="center" color="gray.600">
            <chakra.a color="brand.500"></chakra.a>
          </chakra.p>
        </GridItem>
      </SimpleGrid>
    </Box>
  )
}
