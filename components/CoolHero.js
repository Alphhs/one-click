import {
  Stack,
  Container,
  Box,
  Flex,
  Text,
  Heading,
  SimpleGrid,
  Image,
  Center,
  Button,
  Icon,
} from "@chakra-ui/react"
import HomeBro from "../image/nuur.png"
import Logo from "../image/logo.png"
import {AiFillApple} from "react-icons/ai"
import {RiGooglePlayFill} from "react-icons/ri"

export default function StatsGridWithImage() {
  return (
    <Box
      height={"100vh"}
      display={"flex"}
      alignItems={"center"}
      zIndex={1}
    >
      <Flex
        flex={1}
        zIndex={100}
        display={{base: "none", lg: "flex"}}
        backgroundImage={HomeBro.src}
        backgroundSize={"cover"}
        backgroundPosition="center"
        backgroundRepeat="no-repeat"
        position={"absolute"}
        width={{base: "100%", md: "50%"}}
        insetY={0}
        right={0}
      >
        <Flex
          w={"full"}
          h={"full"}
          bgGradient={{base: "linear(to-r, #1a202c95 100%, transparent)", md: "linear(to-r, #1a202c85 50%, transparent)"}}
        />
      </Flex>
      <Container maxW={"7xl"} zIndex={10} position={"relative"}>
        <Stack mt={{base: "0", md: 0}} direction={{base: "column", lg: "row"}}>
          <Stack
            flex={1}
            color={"gray.400"}
            justify={{lg: "center"}}
            py={{base: 4, md: 20, xl: 60}}
          >
            <Box mb={{base: 12, md: 20}}>
              <Text
                fontFamily={"heading"}
                fontWeight={700}
                textTransform={"uppercase"}
                mb={3}
                fontSize={"xl"}
                textAlign={"center"}
                color={"gray.500"}
              >
                {/* One Click */}
              </Text>
              <Image alt="logo" mb={4} mx={{base: "auto", md: "0px"}} h={"16"} src={Logo.src} />
              <Box maxW={"100%"}></Box>
              <Heading
                // color={"white"}
                mb={12}
                textAlign={{base: "center", md: "left"}}
                fontSize={{base: "4xl", md: "5xl"}}
                className="gradient-title"
              >
                ХЯЛБАР ШИЙД
              </Heading>
              <Text fontSize={"md"} color={"gray.300"} textAlign={"left"}>
                OneClick аппликейшн нь финтек технологи болон хиймэл оюун ухааны
                технологид суурилсан бичил зээлийн үйлчилгээг танд хүргэх
                үйлчилгээ юм.
              </Text>
            </Box>

            <SimpleGrid
              columns={{base: 2}}
              spacing={10}
              justifyContent={"flex-start"}
            >
              {stats.map((stat) => (
                <Button
                  key={stat.title}
                  colorScheme="whiteAlpha"
                  variant="outline"
                  borderWidth={"2px"}
                  size="lg"
                  borderRadius={"xl"}
                  leftIcon={stat.icon}
                  onClick={()=>window.open(`${stat?.content}`, "_blank")}
                >
                  <Text textColor={"white"} fontWeight={"normal"}>
                    {stat.title}
                  </Text>
                </Button>
              ))}
            </SimpleGrid>
          </Stack>
          <Flex flex={1} />
        </Stack>
      </Container>
    </Box>
  )
}

const stats = [
  {
    title: "App store",
    content:"https://apps.apple.com/mn/app/oneclick-mn/id6461725450",
    icon: <Icon as={AiFillApple} size={"1.2rem"} color={"white"} />,
  },
  {
    title: "Play store",
    content:"https://play.google.com/store/apps/details?id=tanasoft.mazz.oneclick&pcampaignid=web_share",
    icon: <Icon as={RiGooglePlayFill} color={"white"} />,
  },
]
