import React from "react";
import {
  chakra,
  Box,
  Flex,
  useColorModeValue,
  Icon,
  Stack,
} from "@chakra-ui/react";
import { PhoneIcon, AddIcon, WarningIcon } from "@chakra-ui/icons";
import { AiOutlineFileProtect } from "react-icons/ai";

export default function App() {
  const Feature = (props) => {
    return (
      <Flex>
        <Flex shrink={0}></Flex>
        <Box>
          <chakra.dt
            fontSize="lg"
            fontWeight="bold"
            lineHeight="6"
            color={"white"}
            fontFamily={"heading"}
            textTransform={"uppercase"}
          >
            {props.title}
          </chakra.dt>
          <chakra.dd
            fontFamily={"body"}
            mt={2}
            textAlign={{ base: "justify" }}
            color={useColorModeValue("whiteAlpha.700", "gray.400")}
          >
            {props.children}
          </chakra.dd>
        </Box>
      </Flex>
    );
  };
  return (
    <Flex p={{base: 5, lg: 0}} w="auto" justifyContent="center" alignItems="center">
      <Box rounded="xl">
        <Box maxW="7xl" mx="auto" px={{ base: 4, lg: 10 }}>
          <Box textAlign={{ lg: "center" }} >
            <chakra.p
              mt={2}
              fontSize={{ base: "3xl", sm: "4xl" }}
              lineHeight="8"
              fontWeight="extrabold"
              fontFamily={"heading"}
              letterSpacing="tight"
              color={"white"}
            >
              АППЫН ТУХАЙ
            </chakra.p>
            <chakra.p
              mt={4}
              maxW="2xl"
              fontSize={{base: "sm", md: "md"}}
              fontWeight={600}
              mx={{ lg: "auto" }}
              fontFamily={"body"}
              textAlign={{ base: "justify" }}
              color={useColorModeValue("whiteAlpha.900", "gray.400")}
            >
              One Click аппликейшн нь финтек технологи болон хиймэл оюун ухааны
              технологид суурилсан бичил зээлийн үйлчилгээг танд хүргэх
              үйлчилгээ юм.
            </chakra.p>
          </Box>

          <Box mt={50} maxW={"3xl"}>
            <Stack
              spacing={{ base: 10, md: 0 }}
              display={{ md: "grid" }}
              gridTemplateColumns={{ md: "auto" }}
              gridColumnGap={{ md: 8 }}
              gridRowGap={{ md: 5 }}
            >
              <Feature title="Aппликейшн" icon={AiOutlineFileProtect}>
                Та манай аппликейшнийг татан авч өөрийн мэдээллийг оруулснаар
                таны зээлийн лимит тогтох ба олон удаагийн давтамжтай үйлчилгээ
                авахад зээлийн эрх нэмэгдэх зэрэг олон давуу талуудыг ашиглах
                боломжтой.
              </Feature>

              <Feature title="Мэдээлэл" icon={AiOutlineFileProtect}>
                Таны мэдээллийг бид задруулахгүй чанд хадгалах тул та өөрийн
                мэдээллийг үнэн зөв оруулах үүрэгтэй.
              </Feature>

              <Feature title="Үйлчилгээ" icon={AiOutlineFileProtect}>
                Мөн энэхүү үйлчилгээг авахад танд зөвхөн гар утас байхад л
                хангалттай ба барьцаа хөрөнгө шаардахгүй интернэтэд холбогдсон л
                бол 24 цагийн хэзээ ч зээлийн хүсэлтээ явуулан зээл авах
                боломжтой.
              </Feature>
            </Stack>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
}
