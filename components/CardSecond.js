import { Box, Text, Button, Divider } from "@chakra-ui/react";
import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
} from "@chakra-ui/react";
import { useState } from "react";

export default function Pricing() {
  const [sliderValue, setSliderValue] = useState(450000);
  const [sliderValue1, setSliderValue1] = useState(2);
  const [sliderValue3, setSliderValue3] = useState(2);

  return (
    <Box
      display={"flex"}
      flexDir="column"
      boxShadow={"light-lg"}
      bg={"gray.800"}
      rounded={"3xl"}
      mx={{base: 3, md: "auto"}}
      maxW={{md: "lg"}}
      minW={{md: "sm", lg: "sm", xl: "xl", "2xl": "2xl"}}
      py="5"
      px={{ xl: 20, base: "8" }}
      mb={"10"}
    >
      <Box
        display={"flex"}
        flexDir="column"
        justifyContent={"center"}
        alignItems="center"
      >
        <Text color={"white"} mt={8} fontWeight={"black"} fontSize={"3xl"}>
          Зээл бодох
        </Text>
        <Slider
          mt={"14"}
          aria-label="slider-ex-1"
          min={100000}
          max={1000000}
          defaultValue={450000}
          onChange={(val) => setSliderValue(val)}
          step="5000"
        >
          <SliderMark
            color={"white"}
            mt="-10"
            value={100000}
            ml="-3"
            fontSize="md"
          >
            {Number(100000).toLocaleString() + "₮"}
          </SliderMark>

          <SliderMark
            color={"white"}
            mt="-10"
            value={1000000}
            ml={{base: -12, md: -20}}
            fontSize="md"
          >
            {Number(1000000).toLocaleString() + "₮"}
          </SliderMark>
          <SliderMark
            value={sliderValue}
            textAlign="center"
            color="white"
            mt="-10"
            ml="-12"
            bgColor={"gray.800"}
            minW={"20"}
          >
            {Number(sliderValue).toLocaleString() + "₮"}
          </SliderMark>
          <SliderTrack borderRadius={"2xl"} h={"2"}>
            <SliderFilledTrack />
          </SliderTrack>
          <SliderThumb />
        </Slider>
        <Slider
          min={1}
          max={"3"}
          mt={"14"}
          aria-label="slider-ex-1"
          defaultValue={2}
          onChange={(val) => setSliderValue1(val)}
        >
          <SliderMark
            color={"white"}
            mt="-10"
            value={1}
            ml="-2.5"
            fontSize="md"
          >
            1 Сар
          </SliderMark>

          <SliderMark color={"white"} mt="-10" value={3}  ml={{base: -10, md: -16}} fontSize="md">
            <Box w={20}>3 Сар</Box>
          </SliderMark>
          <SliderMark
            value={sliderValue1}
            textAlign="center"
            color="white"
            mt="-10"
            ml="-10"
            w="20"
            bgColor={"gray.800"}
          >
            {sliderValue1} Сар
          </SliderMark>
          <SliderTrack borderRadius={"2xl"} h={"2"}>
            <SliderFilledTrack />
          </SliderTrack>
          <SliderThumb />
        </Slider>
        <Slider
          min={1}
          max={6}
          mt={"14"}
          defaultValue="2"
          aria-label="slider-ex-1"
          onChange={(val) => setSliderValue3(val)}
          step={0.5}
        >
          <SliderMark
            color={"white"}
            mt="-10"
            value={1}
            ml="-2.5"
            fontSize="md"
          >
            1%
          </SliderMark>

          <SliderMark
            color={"white"}
            mt="-10"
            value={6}
            ml={{base: -4, md: -6}}
            fontSize="md"
          >
            6%
          </SliderMark>
          <SliderMark
            value={sliderValue3}
            textAlign="center"
            color="white"
            mt="-10"
            ml="-5"
            bgColor={"gray.800"}
            w="12"
          >
            {sliderValue3}%
          </SliderMark>
          <SliderTrack borderRadius={"2xl"} h={"2"}>
            <SliderFilledTrack />
          </SliderTrack>
          <SliderThumb />
        </Slider>
      </Box>
      <Divider my={"5"} />
      <Box
        color={"white"}
        mt={"5"}
        display="flex"
        fontSize={24}
        justifyContent={"space-between"}
      >
        <Text>Нийт төлөлт : </Text>
        <Text>
          {Number(
            (sliderValue * (sliderValue1 * sliderValue3)) / 100 + sliderValue
          ).toLocaleString() + "₮"}
        </Text>
      </Box>
      <Button alignSelf={"center"} mt={"5"} mb={"5"} w={"min-content"} variant="solid">
        Зээл авах
      </Button>
    </Box>
  );
}
