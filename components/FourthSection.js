import {Box, chakra, Flex, Heading, SimpleGrid} from "@chakra-ui/react"

const testimonials = [
  {
    name: "Зээлийн хүсэлт хэрхэн явуулах вэ ?",
    role: "Chief Marketing Officer",
    content:
      "Та зээлийн хүсэлтийг биеэр болон онлайн хэлбэрээр явуулж зээлийн хэмжээг шийдвэрлүүлнэ. ",
    avatar:
      "https://images.unsplash.com/photo-1603415526960-f7e0328c63b1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
  },
  {
    name: "Зээлийн хэмжээ болон хугацаа ?",
    role: "Entrepreneur",
    content: `Зээлийн хэмжээг нэмэгдүүлэх бол биеэр ирж судалгаа хийлгэх.
       Зээл эхлэх хэмжээ 100.000-300.000₮ байна.
       Зээлийн хэмжээ нь таны зээлийн түүхээс хамаарч нэмэгдэж болно. `,
    avatar:
      "https://images.unsplash.com/photo-1598550874175-4d0ef436c909?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
  },
  {
    name: "Гэрээ байгуулах ?",
    role: "Movie star",
    content: `  Онлайн болон биеэр ирж гэрээ байгуулах.
       Зээлийн хэмжээнээс хамаарч шаардлагатай бичиг баримттай ирж гэрээг байгуулна.`,
    avatar:
      "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=334&q=80",
  },
  {
    name: "Зээл төлөх ба хугацаа сунгах ?",
    role: "Musician",
    content: ` Зээлийн хугацааг 1 удаа хүү төлөн сунгах боломжтой.
       Зээл төлөлтийг аппын зааврын дагуу төлнө. `,
    avatar:
      "https://images.unsplash.com/photo-1606513542745-97629752a13b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
  },
]

function TestimonialCard(props) {
  const {name, role, content, avatar} = props 
  return (
    <Flex
      className="shadow-stack hover"
      maxW={"640px"}
      direction={{base: "column-reverse", md: "row"}}
      width={"full"}
      rounded={"3xl"}
      p={{base: 4, sm: 5, md: 8}}
      justifyContent={"space-between"}
      position={"relative"}
      bg={"gray.800"}
    >
      <Flex
        direction={"column"}
        textAlign={"left"}
        gap={".5rem"}
        justifyContent={"space-between"}
      >
        <chakra.p
          color={"white"}
          fontFamily={"heading"}
          fontWeight={"bold"}
          fontSize={{base: "md", sm: "md", md: "lg", lg: "xl", xl: "xl"}}
          textTransform={"uppercase"}
        >
          {name}
        </chakra.p>
        <chakra.p
          color={"whiteAlpha.800"}
          fontFamily={"body"}
          fontWeight={"medium"}
          textAlign={{base: "justify"}}
          fontSize={{base: "12px", md: "14px"}}
          pb={4}
        >
          {content}
        </chakra.p>
      </Flex>
    </Flex>
  )
}

export default function GridBlurredBackdrop() {
  return (
    <Flex
      textAlign={"center"}
      justifyContent={"center"}
      direction={"column"}
      width={"full"}
    >
      <Box
        width={{base: "full"}}
        display={"flex"}
        justifyContent={"center"}
        margin={"auto"}
      >
        <Heading
          fontWeight={600}
          fontSize={{base: "2xl", sm: "3xl", md: "3xl", lg: "4xl", xl: "5xl"}}
          mb={{md: "3"}}
          lineHeight={"110%"}
          color={"white"}
          zIndex={2}
          title="ТҮГЭЭМЭЛ АСУУЛТ ХАРИУЛТУУД"
          className="title faq"
        >
          ТҮГЭЭМЭЛ АСУУЛТ ХАРИУЛТУУД
        </Heading>
      </Box>
      <SimpleGrid
        columns={{base: 1, md: 2}}
        spacing={{base: "4", sm: "5", md: "10"}}
        mt={{base: 5, lg: 20, xl: 20}}
        p={{base: 2}}
        px={{base: "6", md: "20"}}
        mx={"auto"}
      >
        {testimonials.map((cardInfo, index) => (
          <TestimonialCard key={index} {...cardInfo} index={index} />
        ))}
      </SimpleGrid>
    </Flex>
  )
}
