import {
  Box,
  ListItem,
  SimpleGrid,
  Stack,
  OrderedList,
  useBreakpoint,
} from "@chakra-ui/react"
import ReactFullpage from "@fullpage/react-fullpage"
import SecondSection from "../components/SecondSection"
import SixthSection from "../components/SixthSection"
import ThirdSection from "../components/ThirdSection"
import FourthSection from "../components/FourthSection"
import FifthSection from "../components/FifthSection"
import ModalCard from "../components/ModalCard"
import CoolHero from "../components/CoolHero"
import Contact from "../components/Contact"
import Pricing from "../components/CardSecond"

export default function Home() {
  const width = useBreakpoint()
  return (
    <div>
      <div className="area">
        <ul className="circles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
      <ReactFullpage
        licenseKey={"YOUR_LICENSE_KEY"}
        navigation
        scrollOverflow={true}
        render={({fullpageApi}) => {
          return (
            <div id="fullpage-wrapper">
              <div className="section section1">
                <CoolHero />
              </div>
              <div className="section">
                <div className="slide">
                  <SecondSection stay={() => fullpageApi.moveSlideRight()} />
                </div>
              </div>
              <div className="section">
                <ThirdSection />
              </div>
              <div className="section">
                <FourthSection />
              </div>
              <div className="section">
                <div className="slide">
                  <FifthSection stay={() => fullpageApi.moveSlideRight()} />
                </div>
                <div className="slide">
                  <Stack>
                    <SimpleGrid
                      onMouseEnter={() => {
                        fullpageApi.setAllowScrolling(false)
                      }}
                      onMouseLeave={() => {
                        fullpageApi.setAllowScrolling(true)
                      }}
                      px={{"2xl": 10, xl: 8, lg: 10}}
                      columns={{xl: 3, lg: 2}}
                      spacing={{xl: 6, base: 0}}
                    >
                      <ModalCard
                        content={"1. Өөрийн хувийн мэдээллээ үнэн зөв үүсгэнэ."}
                      />
                      <ModalCard
                        content={
                          "2. One Click ХХК нь харилцагчийн үүсгэсэн хувийн мэдээллийг хүлээн авч, хадгалан бусдад мэдээллэхгүй."
                        }
                      />
                      <ModalCard
                        content={
                          "3. 21 нас хүрсэн Монгол улсын иргэн байх ба энэ үйлчилгээг зөвхөн Монгол улсад ашиглах боломжтой болохыг анхаарна уу."
                        }
                      />
                      <ModalCard
                        content={
                          "4. Харилцагчийн мэдээлэлд тулгуурлан авах боломжтой зээлийн хэмжээ, зээлийн нөхцөлийг тодорхойлно. Тодорхойлж хэлвэл энэ үйлчилгээ нь богино хугацааны бичил зээл байх ба харилцагч 90 хоног хүртэл эргэн төлөх нөхцөлтэйгээр зээлийг авах боломжтой."
                        }
                      />
                      <ModalCard
                        content={
                          "5. Хэрэв та санаатайгаар хуурамч мэдээлэл илгээсэн бол манай байгууллага танд зээл олгохоос татгалзаж болно. Мөн хуурамч мэдээллээс үүдэн гарах аливаа үр дагавар, хохирлыг харицлагчаас барагдуулна."
                        }
                      />
                      <ModalCard
                        content={
                          "6. Манай байгууллага нь таны мэдээллийн аюулгүй байдлыг хангах, OneClick апп-ыг хууль бус зорилгоор ашиглахаас урьдчилан сэргийлэхийн тулд таны апп-д хандах хандалт, таны хийж байгаа аливаа үйлдлийн түүхийн лог болгон хадгалах, түүнийг хуулийн хүрээнд ашиглана"
                        }
                      />
                      <ModalCard
                        content={
                          "7. OneClick апп-г зориулалт зааврын дагуу ашиглах үүрэгтэй бөгөөд аливаа хууль бус зорилгоор ашиглахыг хатуу хориглоно."
                        }
                      />
                      <ModalCard
                        content={
                          "8. OneClick апп-нд техникийн өөрчлөлт, шинэчлэлт хийх тохиолдолд түр хугацааны зогсолт хийж болох ба үйлчилгээний нөхцлөө өөрчлөх талаархмэдээллийг өөрийн байгууллагын цахим хуудас болон апп-ны танилцуулах хэсэг, харилцагчийн гар утсаар дамжуулан мэдээллэнэ"
                        }
                      />
                      <ModalCard
                        content={
                          "9. Та энэхүү үйлчилгээний нөхцөлийг зөвшөөрч манай аппликейшнийг ашигласан нь манай байгууллагаас зээл олгох талаар таны өмнө ямар нэг үүрэг хүлээхгүй болно. Түүнчлэн энэхүү үйлчилгээний нөхцөлд тусгайлан дурдсанаас бусад асуудлаар манай байгууллагын эсрэг ямарваа нэг шаардлага гаргах эрхийг таньд олгохгүй болно."
                        }
                      />
                    </SimpleGrid>
                  </Stack>
                </div>
              </div>
              <div className="section">
                <div className="slide">
                  <SixthSection stay={() => fullpageApi.moveSlideRight()} />
                </div>
                <div className="slide">
                  <Box
                    onMouseEnter={() => {
                      fullpageApi.setAllowScrolling(false)
                    }}
                    onMouseLeave={() => {
                      fullpageApi.setAllowScrolling(true)
                    }}
                    fontSize={"xl"}
                    textAlign={"justify"}
                    px={{xl: "36", base: 12}}
                    textColor={"white"}
                  >
                    <Stack
                      fontSize={{base: "12px", md: "14px"}}
                      border={"1px solid white"}
                      px={{base: 6, md: 8, lg: 12}}
                      py={10}
                      borderRadius={"3xl"}
                      lineHeight={10}
                    >
                      <ul>
                        <li>
                          OneClick-д нэвтэрч оруулсан мэдээлэл манай мэдээллийн
                          баазад аюулгүй хадгалагдах болно. Таны мэдээллийг бид
                          зөвхөн тантай холбоо барих, цаашдын үйлчилгээнд
                          шинэчлэл хийхэд анхаарах зэрэгт ашиглана.
                        </li>
                        <li>
                          Харилцагч нь OneClick-д бүртгүүлэхээс өмнө
                          Үйлчилгээний нөхцөл болон Нууцлалын бодлоготой бүрэн
                          танилцаж, хувийн мэдээллийг цуглуулах, боловсруулахыг
                          зөвшөөрсөн бол мэдээлэл хариуцагчид цахимаар зөвшөөрөл
                          олгосон гэж үзнэ.
                        </li>
                        <li>
                          Мэдээлэл хариуцагч нь харилцагчийг таньж мэдэх болон
                          зээл авах хүсэлтийг баталгаажуулах зорилгоор дараах
                          мэдээллийг цуглуулж, боловсруулна. Үүнд:
                          <OrderedList
                            pb={"10px"}
                            pt={"10px"}
                            pl={{base: "30px", xl: "40px"}}
                          >
                            <ListItem>
                              OneClick-д бүртгүүлэхэд ашиглаж буй харилцагчийн
                              гар утасны дугаар, регистрийн дугаар.
                            </ListItem>
                            <ListItem>
                              Харилцагчийг баталгаажуулах зорилгоор бүртгүүлсэн
                              гар утасны дугаарт нууц үгийг илгээх бөгөөд тухайн
                              нууц үгийг OneClick-д оруулснаар бүртгэл үүснэ.
                            </ListItem>
                            <ListItem>
                              Онлайн зээлийн үйлчилгээнд хамрагдахын тулд
                              харилцагчийн OneClick-д оруулж буй овог, нэр, РД,
                              утасны дугаар, гэрийн хаяг, ажил байдлын мэдээлэл,
                              санхүүгийн мэдээлэл.
                            </ListItem>
                            <ListItem>
                              Харилцагчийн OneClick-аар авсан онлайн зээл,
                              түүний эргэн төлөлтийн мэдээлэл.
                            </ListItem>
                            <ListItem>
                              Мэдээлэл хариуцагч нь энэ бодлогод зааснаас өөр
                              харилцагчийн хувийн мэдээллийг цуглуулж,
                              боловсруулж, хадгалахыг хориглоно.
                            </ListItem>
                          </OrderedList>
                        </li>
                        <li>
                          Харилцагч нь гэрээ байгуулсны дараа утасны дугаар,
                          овог нэр, регистрийн дугаар зэрэг хувийн мэдээллийг
                          өөрөө өөрчлөх боломжгүй байна. Харилцагч нь утасны
                          дугаараа сольсон тохиолдолд OneClick-аар дамжуулан
                          утасны дугаар солих хүсэлтийг илгээх ба ажлын 2 хоногт
                          харилцагчийн хүсэлтийг шийдвэрлэнэ.
                        </li>
                        <li>
                          OneClick апп нь харилцагчийн утасны камер руу хандах
                          зөвшөөрлийг асуудаг. Ингэснээр харилцагч бүртгүүлэхдээ
                          зураг оруулах боломжтой болно.
                        </li>
                        <li>
                          Харилцагч OneClick-д бүртгүүлснээс хойш 2 сарын
                          хугацаанд зээлийн гэрээ байгуулаагүй тохиолдолд
                          харилцагчийн мэдээллийг Архив, албан хэрэг хөтлөлтийн
                          тухай хуульд заасан хугацаагаар хадгална.
                        </li>
                        <li>
                          Харилцагч нь өөрийн хүсэлтээр бүртгэлээ цуцлуулах
                          хүсэлт гаргасан тохиолдолд дахин OneClick-д бүртгүүлэх
                          боломжгүй тул харилцагчийн утасны дугаар, регистрийн
                          дугаараас бусад мэдээллийг устгана.
                        </li>
                        <li>
                          Харилцагчийн бүртгэл үүсгэх болон гэрээ хийгдэх үед
                          хүчинтэй байсан Нууцлалын бодлогод нэмэлт, өөрчлөлт
                          орсон тохиолдолд өөрчлөгдсөн Нууцлалын бодлогыг
                          хүчинтэйд тооцно.
                        </li>
                      </ul>
                    </Stack>
                  </Box>
                </div>
              </div>
              <div className="section">
                <Contact stay={() => fullpageApi.moveTo(1, 0)} />
              </div>
            </div>
          )
        }}
      />
    </div>
  )
}
